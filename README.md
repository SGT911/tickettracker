# TicketTracker

## Tecnologias

### Frontend - @ikisanti

- Module Federation - MicronFrontend
  
- React - Framework
  
- Vite - Packaging
  

### Backend - @fabergonzalez16 / @SGT911

- Python
  
- Tornado
  
- Sendgrid
  

### Bases de Datos - @SGT911

- Redis - Cache y Parametrizacion
  
- Neo4J - DB Principal
  
- MongoDB - DB Secundaria
  
- S3 - Alamacenamiento de Datos masivos
  

### Herramientas Extras - _Todos_

- RabbitMQ - Colas tareas asincronas
  
- Nginx - Proxy y Proteccion
  
- ElasticSearch - Log y Analiticas
  
- Golang - Tareas de Apoyo
  
- UnitTesting
  

## Requerimientos

### Version Final

Un sistema integral de gestion tickets administrativos los cuales podran integrarse por medio de webhooks, integracion a GitLab y a Discord, ademas de poder llevar el flujo completo de un ticket (Peticion, Aceptacion, Presupuesto, Seguimiento, Prueba, Entrega)

### MVP

Sistema de tickets capaz de hacer una gestion integral para tickets con su flujo completo.

#### Glosario

1. **Flujo completo**:
  
  1. La creacion del ticket (parte Cliente externo)
    
  2. Notificacion de las partes necesarias al ticket (Correo / Discord)
    
  3. Validacion, Votacion y Aceptacion del ticket (Usuario interno con permiso de Aceptacion de Ticket)
    
  4. Notificacion al cliente de la actualizacion del estado del ticket.
    
  5. Notificacion a todos los involucrados al proyecto del ticket.
    
  6. Seguimiento del ticket (Tipo Trello / GitLab, Kanban).
    
  7. Notificacion de la prueba del ticket (El cliente se le notifica cuando puede probar su ticket).
    
  8. Aceptacion del ticket (el cliente o un usuario lider de proyecto puede marcar como terminado).
    
  9. Quemado del ticket (Remover el ticket de la base de datos y guardarlo en un sistema s3 a largo plazo)
    
2. **Ticket**: Una solicitud de un proceso dentro del proceso de desarrollo y soporte, donde este denotado los siguientes parametros: Titulo, Descripcion, Descripcion del incidente, El cliente involucrado, El personal involucrado, El lider de proceso, Evidencia adjunta (archivos), Criterio de Aceptacion, Ciclo, Tipo Ticket, Labels.
  
3. **Ciclo**: Es una delimitacion de tiempo para un ciclo de desarrollo despues de una entrega o antes de la obtencion de requerimientos.
  
4. **Tipo de Ticket**: Discriminador de que tipo de ticket es, por ejemplo: Caracteristica, Bug, Incidente, etc.
  
5. **Labels**: Sistema de dscriminado rapido por color y por nombre.
  
6. **Cliente**: Usuario externo a la aplicacion que crea y supervisa el ticket.
  
7. **Usuario**: Usuario interno que trabajo o administra el proceso de los tickets.
  
8. **Lider de Proyecto**: Usuario con permisos administrativos dentro del proceso de un proyecto.
  
9. **Lider de Proceso**: Usuario con permisos administrativos dentro de un proceso/ticket.
  
10. **Permiso de Validador**: Usuario con permiso a la seccion de validacion y votacion para la aceptacion de tickets.